"""Summary: All the webscraping sources

Returns:
    list: Webscraping sources
"""
sources = ['bleedingcool', 'cbr', 'comicbook', 'comicsbeat',
           'ign', 'nerdist', 'newsarama', 'outhousers']
