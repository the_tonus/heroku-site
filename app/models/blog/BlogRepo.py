""" Repository for Blog Models """
from .Personal import Personal
from .Tech import Tech

BlogRepo = {
    'personal': Personal,
    'tech': Tech
} 
